
function limpiar() {

    document.getElementById('valor').value = "";
    document.getElementById('engan').innerHTML = "";
    document.getElementById('finan').innerHTML = "";
    document.getElementById('pago').innerHTML = "";
}

function calcular() {

    // Calcular Enganche
    let valorAuto = parseInt(document.getElementById('valor').value);
    if (isNaN(valorAuto) || valorAuto <= 0) {

        return alert("Seleccione una opción válida");

    }
    let enganche = valorAuto * .30;

    let parrafo = document.getElementById('engan')

    parrafo.innerHTML = "$" + enganche;

    let res = (valorAuto - (valorAuto * .30));
    parrafo = document.getElementById('finan')
    parrafoPago = document.getElementById('pago')
    
    // Calcular Financiar  
    let meses = parseInt(document.getElementById('planes').value);

    let intereses;

    if (meses == 12) {

        parrafo.innerHTML = "$" + (res * 1.125).toFixed(2);
        parrafoPago.innerHTML = "$" + ((res * 1.125) / meses).toFixed(2);

    } else if (meses == 18) {

        parrafo.innerHTML = "$" + (res * 1.172).toFixed(2);
        parrafoPago.innerHTML = "$" + ((res * 1.172) / meses).toFixed(2);
    } else if (meses == 24) {

        parrafo.innerHTML = "$" + (res * 1.21).toFixed(2);
        parrafoPago.innerHTML = "$" + ((res * 1.21) / meses).toFixed(2);
    } else if (meses == 36) {

        parrafo.innerHTML = "$" + (res * 1.26).toFixed(2);
        parrafoPago.innerHTML = "$" + ((res * 1.26) / meses).toFixed(2);
    } else if (meses == 48) {

        parrafo.innerHTML = "$" + (res * 1.45).toFixed(2);
        parrafoPago.innerHTML = "$" + ((res * 1.45) / meses).toFixed(2);
    } else {

        return alert("Seleccione una opción válida");

    }

}